#!/usr/bin/env python3
import mechanicalsoup
import re
from urllib.parse import urlsplit, urlunsplit, parse_qs, urlencode
from getpass import getpass

def clean_url(url):
    parsed = urlsplit(url)
    return urlunsplit([*parsed[0:3],urlencode({'id':parse_qs(parsed.query)['id'][0]}),''])

def download_resources(browser, done=set()):
    for link in browser.links(url_regex='mod/resource'):
        href = clean_url(link['href'])
        if href not in done:
            done.update([href])
            response = browser.get(href, stream=True)
            try:
                filename = re.findall('filename="([^"]+)"', response.headers['content-disposition'])[0]
            except (IndexError, KeyError):
                filename = link.text
            if filename not in done:
                print("Downloading {} to {} ...".format(href,filename), end='')
                with open(filename, 'wb') as fd:
                    for chunk in response.iter_content(chunk_size=1024*1024):
                        fd.write(chunk)
                        print('.', end='')
                print('done.')
                done.update(filename)
    for link in browser.links(url_regex='mod/(folder|assign)'):
        href = clean_url(link['href'])
        if href not in done:
            browser.open(href)
            done.update([href])
            download_resources(browser, done)

if __name__ == '__main__':

    browser = mechanicalsoup.StatefulBrowser()
    browser.open(input('YorkU Moodle URL: '))
    
    if "ppylogin" in browser.get_url():
        browser.get_current_page()
        browser.select_form()
        browser["mli"] = input('Username: ')
        browser["password"] = getpass()
        response = browser.submit_selected()
        browser.follow_link('https://moodle.yorku.ca/moodle/login/ppylogin.php')
    
    
    download_resources(browser)
