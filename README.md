MOODLE RESOURCE SCRAPER
=======================

This is a simple Moodle resource scraper written in Python. It works with York University's Moodle and supports Passport York authentication - I've used it to download professor's powerpoints and such for offline viewing. It may work with other moodles that do not require authentication, but that has not been tested.

To use:

    pip3 install --user -r requirements.txt
    python3 scraper.py

The script will prompt for the Moodle URL, username, and password.
